-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 22, 2018 at 03:52 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookSwitch`
--

-- --------------------------------------------------------

--
-- Table structure for table `Admin`
--

CREATE TABLE `Admin` (
  `Name` varchar(20) NOT NULL,
  `Id` varchar(20) NOT NULL,
  `psd` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Admin`
--

INSERT INTO `Admin` (`Name`, `Id`, `psd`) VALUES
('Bramhani', '535', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `bookStore`
--

CREATE TABLE `bookStore` (
  `BookName` varchar(20) NOT NULL,
  `AuthorName` varchar(20) NOT NULL,
  `Description` varchar(100) NOT NULL,
  `DonName` varchar(20) NOT NULL,
  `DonId` varchar(20) NOT NULL,
  `DonMail` varchar(50) NOT NULL,
  `imageLink` varchar(100) NOT NULL,
  `BookId` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookStore`
--

INSERT INTO `bookStore` (`BookName`, `AuthorName`, `Description`, `DonName`, `DonId`, `DonMail`, `imageLink`, `BookId`) VALUES
('Chemistry', 'nee', 'intermediate book for jee', 'asritha', '249', 'asrithamulugoju@gmail.com', '', '249Ch');

-- --------------------------------------------------------

--
-- Table structure for table `TakeRecord`
--

CREATE TABLE `TakeRecord` (
  `BookId` varchar(20) NOT NULL,
  `BookName` varchar(20) NOT NULL,
  `takName` varchar(20) NOT NULL,
  `takMail` varchar(40) NOT NULL,
  `takId` varchar(20) NOT NULL,
  `DonId` varchar(20) NOT NULL,
  `DonName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TakeRecord`
--

INSERT INTO `TakeRecord` (`BookId`, `BookName`, `takName`, `takMail`, `takId`, `DonId`, `DonName`) VALUES
('535WOF', 'wings of fire', 'Mulugoju', 'bramhanimulugoju@gmail.com', '15SS1A0599', '15SS1A0535', 'Bramhani Mulugoju');

-- --------------------------------------------------------

--
-- Table structure for table `UserEntry`
--

CREATE TABLE `UserEntry` (
  `IdNo` varchar(15) NOT NULL,
  `password` varchar(10) DEFAULT NULL,
  `rePswd` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `UserEntry`
--

INSERT INTO `UserEntry` (`IdNo`, `password`, `rePswd`) VALUES
('15SS1A0535', 'bramhani', 'bramhani'),
('15SS1A0535', 'bramhani', 'bramhani'),
('15SS1A0535', 'bramhani', 'bramhani'),
('15SS1A0535', 'bramhani', 'bramhani'),
('15SS1A0535', 'bramhani', 'bramhani'),
('15SS1A0535', 'bramhani', 'bramhani'),
('15SS1A0535', 'bramhani', 'bramhani'),
('15SS1A0535', 'bramhani', 'bramhani');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookStore`
--
ALTER TABLE `bookStore`
  ADD PRIMARY KEY (`BookId`);

--
-- Indexes for table `TakeRecord`
--
ALTER TABLE `TakeRecord`
  ADD PRIMARY KEY (`BookId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
